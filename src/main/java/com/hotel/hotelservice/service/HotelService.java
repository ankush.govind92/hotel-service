package com.hotel.hotelservice.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class HotelService {

    public static final String WELCOME_TO_HOTEL_BOOKING = "This is a cicd example";

    public String getWelcomeMessage() {
        return WELCOME_TO_HOTEL_BOOKING;
    }


    public String getHotelById(String id) {

        return "Hotel details by ID";

    }

    public List<String> getHotels() {

        return Arrays.asList("Hotel1", "Hotel2");
    }
}

